using System;

namespace KonturTest
{
    public class EncryptingStream : IStream
    {
        private readonly IStream inputStream;

        private readonly byte[] inputBuffer;

        private readonly Encryptor encryptor;

        private readonly BlockDataStorage encryptedBlocks;

        private bool wasFinalBlock;

        public EncryptingStream(IStream inputStream, Encryptor encryptor, int inputBufferSize = 4096)
        {
            this.inputStream = inputStream;
            this.encryptor = encryptor;
            this.encryptedBlocks = new BlockDataStorage();
            this.inputBuffer = new byte[inputBufferSize];
            this.wasFinalBlock = false;

            this.encryptor.RegisterReceiveDataCallback(EncryptorCallback);
        }

        public int GetData(byte[] buffer, int offset, int count)
        {
            if (count == 0)
            {
                throw new ArgumentException("Count must be greater than 0", nameof(count));
            }

            while (this.encryptedBlocks.TotalDataCount < count && !this.wasFinalBlock)
            {
                var inputRcvd = this.inputStream.GetData(this.inputBuffer, 0, this.inputBuffer.Length);
                this.wasFinalBlock = (inputRcvd == 0);
                encryptor.AddData(this.inputBuffer, 0, inputRcvd, this.wasFinalBlock);
            }

            var encCount = this.encryptedBlocks.GetData(buffer, offset, count);
            return encCount;
        }

        private void EncryptorCallback(byte[] buffer, int offset, int count, bool final)
        {
            this.encryptedBlocks.AddBlock(buffer, offset, count);
        }
    }
}