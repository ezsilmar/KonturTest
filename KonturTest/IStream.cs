﻿namespace KonturTest
{
    public interface IStream
    {
        int GetData(byte[] buffer, int offset, int count);
    }
}