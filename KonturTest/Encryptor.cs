﻿namespace KonturTest
{
    public abstract class Encryptor
    {
        public abstract void AddData(byte[] buffer, int offset, int count, bool final);

        public abstract void RegisterReceiveDataCallback(ReceiveDataCallback callback);

        public delegate void ReceiveDataCallback(byte[] buffer, int offset, int count, bool final);
    }
}