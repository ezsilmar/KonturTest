﻿using System;
using System.Collections.Generic;

namespace KonturTest
{
    internal class BlockDataStorage
    {
        private readonly LinkedList<ArraySegment<byte>> blocks;

        public BlockDataStorage()
        {
            this.blocks = new LinkedList<ArraySegment<byte>>();
            TotalDataCount = 0;
        }

        public int TotalDataCount
        {
            get;
            private set;
        }

        public void AddBlock(byte[] buffer, int offset, int count)
        {
            var block = new byte[count];
            Array.Copy(buffer, offset, block, 0, count);
            this.blocks.AddLast(new ArraySegment<byte>(block));
            TotalDataCount += block.Length;
        }

        public int GetData(byte[] buffer, int offset, int count)
        {
            if (this.blocks.Count == 0 || count == 0)
            {
                return 0;
            }

            var idx = 0;
            ArraySegment<byte> lastCopiedBlock;
            int lastCopiedCount;
            do
            {
                lastCopiedBlock = this.blocks.GetAndRemoveFirst();
                lastCopiedCount = Math.Min(count - idx, lastCopiedBlock.Count);
                Array.Copy(lastCopiedBlock.Array, lastCopiedBlock.Offset, buffer, offset + idx, lastCopiedCount);
                idx += lastCopiedCount;
            }
            while (idx < count && this.blocks.Count > 0);

            if (lastCopiedCount != lastCopiedBlock.Count)
            {
                var trimmedOffset = lastCopiedBlock.Offset + lastCopiedCount;
                var trimmedCount = lastCopiedBlock.Count - lastCopiedCount;
                var trimmedBlock = new ArraySegment<byte>(lastCopiedBlock.Array, trimmedOffset, trimmedCount);
                this.blocks.AddFirst(trimmedBlock);
            }

            TotalDataCount -= idx;
            return idx;
        }
    }
}