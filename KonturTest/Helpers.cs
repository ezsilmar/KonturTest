using System.Collections.Generic;

namespace KonturTest
{
    internal static class Helpers
    {
        public static TElem GetAndRemoveFirst<TElem>(this LinkedList<TElem> list)
        {
            var first = list.First.Value;
            list.RemoveFirst();
            return first;
        }
    }
}