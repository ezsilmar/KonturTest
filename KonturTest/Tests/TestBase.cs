using System.Text;

namespace KonturTest.Tests
{
    internal class TestBase
    {
        protected static string DecodeASCII(byte[] buffer, int offset, int count)
        {
            return Encoding.ASCII.GetString(buffer, offset, count);
        }

        protected static byte[] EncodeASCII(string s)
        {
            return Encoding.ASCII.GetBytes(s);
        }
    }
}