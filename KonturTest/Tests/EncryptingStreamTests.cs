﻿using System;
using NSubstitute;
using NUnit.Framework;

namespace KonturTest.Tests
{
    [TestFixture]
    internal class EncryptingStreamTests : TestBase
    {
        [Test]
        public void EmptyInput_CallsEncryptorOnce_WithEmptyFinalBlock()
        {
            var stream = Substitute.For<IStream>();
            stream.GetData(null, 0, 0).ReturnsForAnyArgs(0);
            var enc = Substitute.For<Encryptor>();

            var encStream = new EncryptingStream(stream, enc, 10);
            var buffer = new byte[] {42};
            var rcvd = encStream.GetData(buffer, 0, 1);
            Assert.That(rcvd, Is.EqualTo(0));
            Assert.That(buffer[0], Is.EqualTo(42));

            stream.ReceivedWithAnyArgs(1).GetData(null, 0, 0);
            enc.Received(1).AddData(Arg.Any<byte[]>(), Arg.Any<int>(), Arg.Is(0), true);
        }

        [Test]
        public void InCtor_ReceiveDataCallback_IsRegisteredOnce()
        {
            var encryptor = Substitute.For<Encryptor>();
            var encStream = new EncryptingStream(Substitute.For<IStream>(), encryptor);
            encryptor.ReceivedWithAnyArgs(1).RegisterReceiveDataCallback(null);
        }

        [Test]
        public void GetDataWithZeroCount_ThrowsArgumentException()
        {
            var encStream = new EncryptingStream(Substitute.For<IStream>(), Substitute.For<Encryptor>());

            Assert.Throws<ArgumentException>(() => encStream.GetData(new byte[3], 1, 0));
        }

        [Test]
        public void GetData_ReturnsEncryptedInput()
        {
            var stream = Substitute.For<IStream>();
            stream.GetData(null, 0, 0).ReturnsForAnyArgs(1, 2, 0);

            var encryptor = Substitute.For<Encryptor>();
            Encryptor.ReceiveDataCallback clbk = null;
            encryptor.RegisterReceiveDataCallback(Arg.Do<Encryptor.ReceiveDataCallback>(c => clbk = c));
            encryptor
                .WhenForAnyArgs(e => e.AddData(null, 0, 0, false))
                .Do(Callback
                    .First(ci => clbk(EncodeASCII("abc"), 0, 3, false))
                    .Then(
                        ci =>
                        {
                            clbk(EncodeASCII("d"), 0, 1, false);
                            clbk(EncodeASCII("e"), 0, 1, false);
                        }
                    )
                    .Then(ci => clbk(new byte[0], 0, 0, true))
                    .ThenKeepDoing(ci => Assert.Fail()));

            var encStream = new EncryptingStream(stream, encryptor);
            var buffer = new byte[4];
            var encryptedData = string.Empty;
            var rcvd = 0;
            do
            {
                encryptedData += DecodeASCII(buffer, 0, rcvd);
                rcvd = encStream.GetData(buffer, 0, buffer.Length);
            }
            while (rcvd > 0);

            Assert.That(encryptedData, Is.EqualTo("abcde"));
            encryptor.ReceivedWithAnyArgs(3).AddData(null, 0, 0, false);
            stream.ReceivedWithAnyArgs(3).GetData(null, 0, 0);
        }
    }
}