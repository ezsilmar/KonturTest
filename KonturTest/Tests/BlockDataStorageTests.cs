﻿using System.Linq;
using NUnit.Framework;

namespace KonturTest.Tests
{
    [TestFixture]
    internal class BlockDataStorageTests : TestBase
    {
        [Test]
        public void UseCase()
        {
            var blocks = new[] {EncodeASCII("abc"), EncodeASCII("de"), EncodeASCII("qw")};
            var storage = new BlockDataStorage();
            var buffer = new byte[10];

            Assert.That(storage.GetData(buffer, 0, buffer.Length), Is.EqualTo(0));
            Assert.That(storage.TotalDataCount, Is.EqualTo(0));

            storage.AddBlock(blocks[0], 0, blocks[0].Length);
            Assert.That(storage.TotalDataCount, Is.EqualTo(blocks[0].Length));

            Assert.That(storage.GetData(buffer, 0, buffer.Length), Is.EqualTo(3));
            Assert.That(DecodeASCII(buffer, 0, 3), Is.EqualTo("abc"));

            storage.AddBlock(blocks[1], 0, blocks[1].Length);
            Assert.That(storage.TotalDataCount, Is.EqualTo(blocks[1].Length));
            storage.AddBlock(blocks[2], 0, blocks[2].Length);
            Assert.That(storage.TotalDataCount, Is.EqualTo(blocks[1].Length + blocks[2].Length));

            Assert.That(storage.GetData(buffer, 0, 1), Is.EqualTo(1));
            Assert.That(DecodeASCII(buffer, 0, 1), Is.EqualTo("d"));
            Assert.That(storage.TotalDataCount, Is.EqualTo(3));

            Assert.That(storage.GetData(buffer, 0, 3), Is.EqualTo(3));
            Assert.That(DecodeASCII(buffer, 0, 3), Is.EqualTo("eqw"));

            Assert.That(storage.TotalDataCount, Is.EqualTo(0));
            Assert.That(storage.GetData(buffer, 0, buffer.Length), Is.EqualTo(0));
        }

        [Test]
        public void TotalDataCount_IsSumOfCurrentBlocksLength()
        {
            var storage = new BlockDataStorage();
            storage.AddBlock(new byte[3], 0, 3);
            storage.AddBlock(new byte[4], 0, 4);
            storage.GetData(new byte[5], 0, 5);

            Assert.That(storage.TotalDataCount, Is.EqualTo(2));
        }

        [Test]
        public void NoBlocks_TotalDataCountIsZero()
        {
            var storage = new BlockDataStorage();

            Assert.That(storage.TotalDataCount, Is.EqualTo(0));
        }

        [Test]
        public void NoBlocks_GetDataReturnsZero()
        {
            var storage = new BlockDataStorage();

            Assert.That(storage.GetData(new byte[3], 0, 3), Is.EqualTo(0));
        }

        [Test]
        public void RequestedCountIsSmall_BlocksAreSplitted()
        {
            var storage = new BlockDataStorage();
            storage.AddBlock(EncodeASCII("abcdef"), 0, 6);
            var buffer = new byte[3];

            Assert.That(storage.GetData(buffer, 0, 3), Is.EqualTo(3));
            Assert.That(DecodeASCII(buffer, 0, 3), Is.EqualTo("abc"));

            Assert.That(storage.GetData(buffer, 0, 3), Is.EqualTo(3));
            Assert.That(DecodeASCII(buffer, 0, 3), Is.EqualTo("def"));
        }

        [Test]
        public void RequestedCountIsLarge_BlocksAreConcatenated()
        {
            var blocks = new[] {"abc", "def", "qw"}.Select(EncodeASCII).ToArray();
            var storage = new BlockDataStorage();
            var buffer = new byte[10];
            foreach (var block in blocks)
            {
                storage.AddBlock(block, 0, block.Length);
            }

            Assert.That(storage.GetData(buffer, 0, buffer.Length), Is.EqualTo(8));
            Assert.That(DecodeASCII(buffer, 0, 8), Is.EqualTo("abcdefqw"));
        }
    }
}